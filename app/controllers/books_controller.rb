class BooksController < ApplicationController
	before_action :authenticate_model!

	def show
	end
	
	def new
		@book = Book.new
	
	end

	def index
		@books =Book.all
		respond_to do |format|
			format.html
			format.csv { send_data text: @books.to_csv }
			format.xls #{ send_data text: @books.to_csv(col_sep: "\t") }
		end
	end
	

	def create
		@book = Book.create(params[:book].permit(:name, :password))
		@book.save
		render :text => 'successfull'
	end

	def import
		book.import(params[:file])
		redirect_to root_url, notice: "books imported."
		
	end

end
